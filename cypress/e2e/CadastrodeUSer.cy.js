describe('Recarga online', () => {
    const data = require('../fixtures/User.json');
    let token;
    let id;
    let idRefeicao;
    const filePath = '../../Captura de Tela (22).pdf'
    before(() => {
        cy.token(data.matricula, data.password).then((tkn) => {token = tkn;
        });
    })
    it('Cadastrar user com o mesmo número de telefone', () => {
        cy.request({
            method: 'POST',
            url: '/administracao/usuarios',
            headers: {
                authorization: `Bearer ${token}`,
                'Content-Type': 'multipart/form-data',
              },
            body : {
                "Nome" : "A",
                "NomeSocial" : "",
                "Email" : "cuaze@gmail.com",
                "CPF" : "",
                "Telefone" : "982309576",
                "NumeroCartao" : "",
                "Vegetariano" : true,
                "RestricaoLactose" : true,
                "RestricaoGluten" : true,
                "RestricaoFrutosDoMar" : true,
                "RestricaoDiabetes" : true,
                "TemOutrasRestricoes" : false,
                "OutrasRestricoes" : "",
                "Foto" : "",
                "Saldo" : "",
                Indentificacoes: {
                    "id": 0,
                    "numeroIdentificacao": 0,
                    "identificacaoTipoId": 0,
                    "situacaoId": 0,
                    "cursoId": 0,
                    "gruposAcesso": [
                      {
                        "grupoAcessoId": 0
                      }
                    ]
                }
            }
        })
    });
})